import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // avant java 5
        List lst = new ArrayList();
        lst.add("azerty");
        String str = (String) lst.get(0);

        // après java 5 avec les type generique
        List<String> lstGen = new ArrayList<>();
        lstGen.add("azerty");
        str = lstGen.get(0);

        // Classe Générique
        Box<Integer, Double> b1 = new Box<>(3, 5.4);
        System.out.println(b1);

        Box<String, Integer> b2 = new Box<>("azerty", 5);
        System.out.println(b2);
        
        try {
            b2.Traitement(String.class);
        } catch (InstantiationException e) {
                     e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        // Méthode générique
        System.out.println(Main.<String>egal("azerty", "azerty"));
        System.out.println(egal("azerty", "aaaaa"));

        System.out.println(Main.<Integer>egal(23, 25));
        System.out.println(egal(42, 42));
        
        // Wilcard ?
        List<String> lstStr=new ArrayList<>();
        lstStr.add("Bonjour");
        lstStr.add("Hello");
        afficher(lstStr);
        
    }

    public static <T> boolean egal(T v1, T v2) {
        return v1.equals(v2);
    }
    
    public static void afficher(List<?> lst) {
        //lst.add("aze");
        for(Object o : lst) {
            System.out.println(o);
        }
    }

}
