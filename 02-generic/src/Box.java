
public class Box<T, S> {
    private T a;

    private S b;

    public Box(T a, S b) {
        this.a = a;
        this.b = b;
    }

    public T getA() {
        return a;
    }

    public void setA(T a) {
        this.a = a;
    }

    public S getB() {
        return b;
    }

    public void setB(S b) {
        this.b = b;
    }
    
    public T Traitement(Class<T> clazz) throws InstantiationException, IllegalAccessException{
      //  T tmp=new T();
        return clazz.newInstance(); 
    }

    @Override
    public String toString() {
        return "Box [a=" + a + ", b=" + b + "]";
    }

}
