package fr.dawan.formation.maven1;

import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import junit.framework.TestCase;

public class CalculTest extends TestCase {

    @Test
    public void testSomme() {
        Calcul c = new Calcul();
        assertEquals(6.0, c.addition(2.0, 4.0));
        assertNotEquals(0.0, c.addition(1.0, 4.0));
    }

    @Test
    public void testDivision() throws ArithmeticException {
        Calcul c = new Calcul();
        assertEquals(3.0, c.division(6.0, 2.0));
    }

    @Test
    public void testDivZero() {
        try {
            Calcul c = new Calcul();
            c.division(3.0, 0.0);
            assertTrue(false);
        } catch (ArithmeticException e) {
            assertTrue(true);
        }
    }
}
