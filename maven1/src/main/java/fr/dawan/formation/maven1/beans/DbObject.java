package fr.dawan.formation.maven1.beans;

import java.io.Serializable;

public abstract class DbObject implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "id=" + id;
    }

}
