package fr.dawan.formation.maven1.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import fr.dawan.formation.maven1.beans.DbObject;

public abstract class GenericDao<T extends DbObject> {

    private static Connection cnx = null;

    public void saveOrUpdate(T c,boolean close ) throws SQLException, IOException {
        if (c.getId() == 0) {
            create(c,getConnection());
        } else {
            update(c,getConnection());
        }
        closeConnection(close);
    }
    
    public void delete(T c,boolean close) throws SQLException, IOException {
        delete(c,getConnection());
        closeConnection(close);
    }
    
    public T find(long id,boolean close) throws SQLException, IOException {
        T elm=find(id,getConnection());
        closeConnection(close);
        return elm; 
    }
    
    public List<T> findAll(boolean close) throws SQLException, IOException{
        List<T> lst=findAll(getConnection());
        closeConnection(close);
        return lst;
    }

    protected abstract void create(T elm,Connection cnx) throws SQLException;

    protected abstract void update(T elm,Connection cnx) throws SQLException;
    
    protected abstract void delete(T elm,Connection cnx) throws SQLException;
    
    protected abstract T find(long id,Connection cnx) throws SQLException;
    
    protected abstract List<T> findAll(Connection cnx) throws SQLException;

    protected Connection getConnection() throws SQLException, IOException {
        if (cnx == null) {
            Properties conf = new Properties();
            // Pour charger le fichier de prooriété dans le dosier src/main/resources
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            conf.load(loader.getResourceAsStream("mysql.properties"));
            cnx = DriverManager.getConnection(conf.getProperty("url"), conf.getProperty("user"),
                    conf.getProperty("password"));
        }
        return cnx;
    }
    
    protected void closeConnection(boolean close) throws SQLException {
        if(close && cnx!=null)
        {
            cnx.close();
        }
    }
}
