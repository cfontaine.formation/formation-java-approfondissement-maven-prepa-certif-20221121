package fr.dawan.formation.maven1.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.formation.maven1.beans.ContactGen;

public class ContactGenDao extends GenericDao<ContactGen> {

    @Override
    protected void create(ContactGen c, Connection cnx) throws SQLException {
        String req = "INSERT INTO contact(prenom,nom,date_naissance,email) VALUES(?,?,?,?)";
        PreparedStatement ps = cnx.prepareStatement(req, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, c.getPrenom());
        ps.setString(2, c.getNom());
        ps.setDate(3, Date.valueOf(c.getDateNaissance()));
        ps.setString(4, c.getEmail());
        ps.executeUpdate();

        ResultSet r = ps.getGeneratedKeys();
        if (r.next()) {
            long id = r.getLong(1);
            c.setId(id);
        }

    }

    @Override
    protected void update(ContactGen c, Connection cnx) throws SQLException {
        String req = "UPDATE contact SET prenom=?,nom=?,date_naissance=?,email=? WHERE id=?";
        PreparedStatement ps = cnx.prepareStatement(req);
        ps.setString(1, c.getPrenom());
        ps.setString(2, c.getNom());
        ps.setDate(3, Date.valueOf(c.getDateNaissance()));
        ps.setString(4, c.getEmail());
        ps.setLong(5, c.getId());
        ps.executeUpdate();
    }

    @Override
    protected void delete(ContactGen elm, Connection cnx) throws SQLException {
        String req = "DELETE FROM contact WHERE id=?";
        PreparedStatement ps = cnx.prepareStatement(req);
        ps.setLong(1, elm.getId());
        ps.executeUpdate();
    }

    @Override
    protected ContactGen find(long id, Connection cnx) throws SQLException {
        ContactGen c = null;
        String req = "SELECT prenom,nom,date_naissance,email FROM contact WHERE id=?";
        PreparedStatement ps = cnx.prepareStatement(req);
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            c = new ContactGen(rs.getString("prenom"), rs.getString("nom"), rs.getDate("date_naissance").toLocalDate(),
                    rs.getString("email"));
            c.setId(id);
        }
        return c;
    }

    @Override
    protected List<ContactGen> findAll(Connection cnx) throws SQLException {
        List<ContactGen> lst = new ArrayList<>();
        String req = "SELECT id,prenom,nom,date_naissance,email FROM contact";
        PreparedStatement ps = cnx.prepareStatement(req);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            ContactGen c = new ContactGen(rs.getString("prenom"), rs.getString("nom"),
                    rs.getDate("date_naissance").toLocalDate(), rs.getString("email"));
            c.setId(rs.getLong("id"));
            lst.add(c);
        }
        return lst;
    }

}
