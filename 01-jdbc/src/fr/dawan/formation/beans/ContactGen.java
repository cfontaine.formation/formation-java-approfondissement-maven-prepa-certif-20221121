package fr.dawan.formation.beans;
import java.time.LocalDate;

public class ContactGen extends DbObject  {

    private static final long serialVersionUID = 1L;
    
    private String prenom;
    
    private String nom;
    
    private LocalDate dateNaissance;
    
    private String email;

    public ContactGen() {

    }

    public ContactGen(String prenom, String nom, LocalDate dateNaissance, String email) {   
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.email = email;
    }


    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Contact [prenom=" + prenom + ", nom=" + nom + ", dateNaissance=" + dateNaissance
                + ", email=" + email + "]";
    }

}
