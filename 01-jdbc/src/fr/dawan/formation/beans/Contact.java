package fr.dawan.formation.beans;

import java.io.Serializable;
import java.time.LocalDate;

//JavaBean => un objet java qui respecte 4 régles (après, on peut ajouter toutes les méthodes, constructeurs que l'on veut) 
//1. un moyen de sérialisation (généralement, implémente java.io.Serializable)
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    // 2. des attributs privés
    private long id;

    private String prenom;

    private String nom;

    private LocalDate dateNaissance;

    private String email;

    // 3. un constructeur public sans arguments
    public Contact() {

    }

    public Contact(String prenom, String nom, LocalDate dateNaissance, String email) {
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.email = email;
    }

    // 4. des getters et setters pour chaque attribut
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Contact [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", dateNaissance=" + dateNaissance
                + ", email=" + email + "]";
    }

}
