package fr.dawan.formation;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.List;

import fr.dawan.formation.beans.Contact;
import fr.dawan.formation.beans.ContactGen;
import fr.dawan.formation.dao.ContactGenDao;

public class Main {

    public static void main(String[] args) {
        // Test de JDBC
//        testConnection();

        // Test persistence d'un objet
//        Contact c1=new Contact("Alan", "Smithee",LocalDate.of(1980, 12, 21),"asmithhe@dawan.fr"); 
//        System.out.println(c1);
//        saveContact(c1);
//        System.out.println(c1);

        // Test Dao
//        try {
//
//            Contact c2 = new Contact("Jane", "Doe", LocalDate.of(1980, 12, 21), "jad@dawan.fr");
//            ContactDao dao = new ContactDao();
//
//            System.out.println("_____SaveOrUpdate (Insert)_______");
//            System.out.println(c2);
//            dao.saveOrUpdate(c2);
//            System.out.println(c2);
//
//            System.out.println("_____FindById_______");
//            long id = c2.getId();
//            Contact c3 = dao.find(id);
//            System.out.println(c3);
//
//            System.out.println("_____SaveOrUpdate (Update)_______");
//            c2.setEmail("othermail@dawan.com");
//            dao.saveOrUpdate(c2);
//
//            System.out.println("_____FindAll_______");
//            List<Contact> lst = dao.findAll();
//            for (Contact co : lst) {
//                System.out.println(co);
//            }
//
//            System.out.println("_____Delete_______");
//            dao.delete(c2);
//
//            lst = dao.findAll();
//            for (Contact co : lst) {
//                System.out.println(co);
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        
        // Test GenericDao
        try {

            ContactGen c2 = new ContactGen("Jane", "Doe", LocalDate.of(1980, 12, 21), "jad@dawan.fr");
            ContactGenDao dao = new ContactGenDao();

            System.out.println("_____SaveOrUpdate (Insert)_______");
            System.out.println(c2);
            dao.saveOrUpdate(c2,false);
            System.out.println(c2);

            System.out.println("_____FindById_______");
            long id = c2.getId();
            ContactGen c3 = dao.find(id,false);
            System.out.println(c3);

            System.out.println("_____SaveOrUpdate (Update)_______");
            c2.setEmail("othermail@dawan.com");
            dao.saveOrUpdate(c2,false);

            System.out.println("_____FindAll_______");
            List<ContactGen> lst = dao.findAll(false);
            for (ContactGen co : lst) {
                System.out.println(co);
            }

            System.out.println("_____Delete_______");
            dao.delete(c2,false);

            lst = dao.findAll(true);
            for (ContactGen co : lst) {
                System.out.println(co);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    

    public static void testConnection() {
        try {
            // Chargement du pilote de la base de de données
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Ouverture d’une connexion à la base de donnée
            Connection cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/formation", "root", "dawan");

            // Statement => permet d'exécuter une requète SQL
            Statement stm = cnx.createStatement();

            // Il faut utiliser la méthode executeUpdate pour les requêtes INSERT, DELETE,
            // UPDATE
            String req = "INSERT INTO contact(prenom,nom,date_naissance,email) VALUES('John','Doe','1992/04/05','jd@dawan.com')";
            stm.executeUpdate(req);

            // Il faut utiliser la méthode executeQuery pour les requêtes SELECT
            // ResultSet => pour récupèrer les résultats de la requête
            req = "SELECT id,prenom,nom,date_naissance,email FROM contact";
            ResultSet r = stm.executeQuery(req);
            while (r.next()) {
                // Les Méthodes getString, getDouble, ... permettent de lire les valeurs des
                // colonnes

                System.out.println(r.getLong("id") + " " + r.getString("prenom") + " " + r.getString("nom") + " "
                        + r.getDate("date_naissance") + " " + r.getString("email"));
            }

            cnx.close(); // Fermeture de la connexion à la base de donnée
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void saveContact(Contact c) {
        try {
            // à partir de JDBC 4, il n'est plus necessaire de charger le driver avec
            // Class.forName
            Connection cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/formation", "root", "dawan");

            String req = "INSERT INTO contact(prenom,nom,date_naissance,email) VALUES(?,?,?,?)";

            // PrepraredStatement => permet d'exécuter aussi une requête SQL
            // à privilégier, si la requête contient des paramètres: permet d'éviter les
            // concaténations, les types sont vérifiés ...
            PreparedStatement ps = cnx.prepareStatement(req, Statement.RETURN_GENERATED_KEYS);
            // -> Statement.RETURN_GENERATED_KEYS permet de récupérer la clé primaire généré
            // par la bdd

            // Les méthodes setString, setDouble ... permettent de donnée des valeurs aux
            // paramètres
            // L'indice commence à 1
            ps.setString(1, c.getPrenom());
            ps.setString(2, c.getNom());
            ps.setDate(3, Date.valueOf(c.getDateNaissance()));
            ps.setString(4, c.getEmail());
            ps.executeUpdate();

            // Récupérer la valeur de la clé primaire générée par la base de donnée
            ResultSet r = ps.getGeneratedKeys();
            if (r.next()) {
                long id = r.getLong(1);
                c.setId(id);
            }
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void TestTransaction(Contact c) {
        Connection cnx = null;
        try {
            cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/formation", "root", "dawan");

            // Désactivation du mode auto-commit
            cnx.setAutoCommit(false);

            String req = "INSERT INTO contact(prenom,nom,date_naissance,email) VALUES(?,?,?,?)";
            PreparedStatement ps = cnx.prepareStatement(req, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, c.getPrenom());
            ps.setString(2, c.getNom());
            ps.setDate(3, Date.valueOf(c.getDateNaissance()));
            ps.setString(4, c.getEmail());
            ps.executeUpdate();
            ResultSet r = ps.getGeneratedKeys();
            if (r.next()) {
                long id = r.getLong(1);
                c.setId(id);
            }
            cnx.commit(); // commit()=> => permet de valider les requêtes
        } catch (SQLException e) {
            try {
                cnx.rollback(); // rollback() pour annuler les requêtes
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                cnx.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
