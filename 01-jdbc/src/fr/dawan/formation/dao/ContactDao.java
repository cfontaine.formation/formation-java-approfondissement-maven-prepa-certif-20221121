package fr.dawan.formation.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import fr.dawan.formation.beans.Contact;

public class ContactDao {

    // Persister ou mettre à jour un objet Contact
    public void saveOrUpdate(Contact c) throws SQLException, IOException {
        // Si un contact n'est pas encore persisté => id=0
        if (c.getId() == 0) {
            create(c);
        } else {
            update(c);
        }
    }

    // Effacer un contact de la table contact
    public void delete(Contact c) throws SQLException, IOException {
        Connection cnx = getConnection();
        String req = "DELETE FROM contact WHERE id=?";
        PreparedStatement ps = cnx.prepareStatement(req);
        ps.setLong(1, c.getId());
        ps.executeUpdate();
        cnx.close();
    }

    // Récupérer un contact dans la table à partir de son id
    public Contact find(long id) throws SQLException, IOException {
        Contact c = null;
        Connection cnx = getConnection();
        String req = "SELECT prenom,nom,date_naissance,email FROM contact WHERE id=?";
        PreparedStatement ps = cnx.prepareStatement(req);
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            c = new Contact(rs.getString("prenom"), rs.getString("nom"), rs.getDate("date_naissance").toLocalDate(),
                    rs.getString("email"));
            c.setId(id);
        }
        cnx.close();
        return c;
    }

    // Récupérer tous les contacts dans la table contact
    public List<Contact> findAll() throws SQLException, IOException {
        List<Contact> lst = new ArrayList<>();
        
        // On peut utiliser un try with ressource pour fermer automatiquement la connexion à la base de donnée
        try (Connection cnx = getConnection()) {
            String req = "SELECT id,prenom,nom,date_naissance,email FROM contact";
            PreparedStatement ps = cnx.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Contact c = new Contact(rs.getString("prenom"), rs.getString("nom"),
                        rs.getDate("date_naissance").toLocalDate(), rs.getString("email"));
                c.setId(rs.getLong("id"));
                lst.add(c);
            }
        }
        return lst;
    }

    // Ajouter un contact dans la table contact
    private void create(Contact c) throws SQLException, IOException {
        Connection cnx = getConnection();
        String req = "INSERT INTO contact(prenom,nom,date_naissance,email) VALUES(?,?,?,?)";
        PreparedStatement ps = cnx.prepareStatement(req, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, c.getPrenom());
        ps.setString(2, c.getNom());
        ps.setDate(3, Date.valueOf(c.getDateNaissance()));
        ps.setString(4, c.getEmail());
        ps.executeUpdate();

        ResultSet r = ps.getGeneratedKeys();
        if (r.next()) {
            long id = r.getLong(1); // "id"
            c.setId(id);
        }
        cnx.close();
    }

    // Mettre à jour un contact dans la table contact
    private void update(Contact c) throws SQLException, IOException {
        Connection cnx = getConnection();
        String req = "UPDATE contact SET prenom=?,nom=?,date_naissance=?,email=? WHERE id=?";
        PreparedStatement ps = cnx.prepareStatement(req);
        ps.setString(1, c.getPrenom());
        ps.setString(2, c.getNom());
        ps.setDate(3, Date.valueOf(c.getDateNaissance()));
        ps.setString(4, c.getEmail());
        ps.setLong(5, c.getId());
        ps.executeUpdate();
        cnx.close();
    }

    // Créer la connexion à la base de donnée
    private Connection getConnection() throws SQLException, IOException {
        // Les paramètres de connexion de la base de données sont stockés dans le fichiers de propriété : mysql.properties
        // il doit être placé à la racine du projet
        Properties conf = new Properties();
        conf.load(new FileReader("mysql.properties"));
        return DriverManager.getConnection(conf.getProperty("url"), conf.getProperty("user"),
                conf.getProperty("password"));
    }
}
