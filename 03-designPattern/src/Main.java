
public class Main {

    public static void main(String[] args) {
       Singleton s1=Singleton.getInstance();
       System.out.println(s1 + " " +s1.getData());
       s1.setData(42);
       
       //
       Singleton s2=Singleton.getInstance();
       System.out.println(s2 + " " +s2.getData());
       //Singleton s3=new Singleton();
    }

}
