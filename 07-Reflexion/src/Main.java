import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class Main {

    public static void main(String[] args) {
        // 1
        Class<String> c1 = String.class;

        // 2
        String str = "hello";
        Class<? extends String> c2 = str.getClass();

        // 3
        try {
            Class<?> c3 = Class.forName("java.lang.String");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // Nom de la classe
        System.out.println(c1.getName());

        System.out.println(c1.getSuperclass().getName());

        // Attributs
        Field[] attr = c1.getDeclaredFields();
        for (Field f : attr) {
            System.out.println(f.getName());

        }

        // Méthode
        Method[] meth = c1.getDeclaredMethods();
        for (Method m : meth) {
            System.out.println(m.getName());
            System.out.println(m.getReturnType().getName());
            Parameter[] param = m.getParameters();
            for (Parameter p : param) {
                System.out.println(p.getName() + " " + p.getType().getName());
            }
        }

        Constructor[] construct = c1.getConstructors();
        for (Constructor c : construct) {
            System.out.println(c);
        }

        // Instensitation Dynamique
        try {
            // Avec le constructeur par défaut
            String strdefault = c1.newInstance();
            System.out.println(strdefault);

            // Avec le constructeur qui une chaine de caractère en paramètre
            Constructor<String> constructeur;

            constructeur = c1.getConstructor(new Class[] { String.class });
            String strString=constructeur.newInstance(new Object[]{"Test"});
            System.out.println(strString);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
