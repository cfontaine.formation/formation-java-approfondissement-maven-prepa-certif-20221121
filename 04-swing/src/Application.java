import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Application {

    public static void main(String[] args) {
        // Création d'une fenêtre (conteneur)
        JFrame win = new JFrame();
        // Ajout d'un titre à la fenêtre (peut aussi être définie avec le constructeur
        win.setTitle("Exemple Swing");
        // Définir les dimensions de la fenêtre
        win.setSize(800, 600);
        // Définir la position de la fenêtre
        win.setLocation(200, 200);
        // Définir une dimmension minimum pour fenêtre
        win.setMinimumSize(new Dimension(320, 200));
        // Configurer le comportement de la fenêtre à la fermeture
        // Arrét du programme lorsque l'on ferme la fenêtre
        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Création d'un bouton (composant)
        JButton bp1 = new JButton("Ok");
        JButton bp2 = new JButton("Not Ok");

        // Layout Manager -> positionnement des composants

        // Positionnement absolue
//        win.setLayout(null);
//        bp1.setBounds(500, 500, 100, 30);
//        bp2.setBounds(30,250,200,120);

        // FlowLayout
//        win.setLayout(new FlowLayout(FlowLayout.LEFT, 100, 50));

        // BorderLayout
//        win.setLayout(new BorderLayout());
//        win.getContentPane().add(bp1,BorderLayout.LINE_START);
//        win.getContentPane().add(bp2,BorderLayout.PAGE_START);

        // GridLayout
//        win.setLayout(new GridLayout(3,4,10,50));
//        for(int i=0;i<12;i++) {
//            win.getContentPane().add(new JButton("bp "+i));
//       }

//        Ajout des boutons au contentPane de la fenêtre
//        win.getContentPane().add(bp1);
//        win.getContentPane().add(bp2);

        // Panneau -> JPanel (conteneur)
        JPanel pan = new JPanel();
        pan.add(bp1);
        pan.add(bp2);
        win.add(pan, BorderLayout.PAGE_END);

        // ActionListener
        bp1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // JOptionPane => boite de dialogue
                JOptionPane.showMessageDialog(win, "Bouton 1", "Titre", JOptionPane.INFORMATION_MESSAGE);
                int choix = JOptionPane.showConfirmDialog(win, "Message de la boite de dialog", "Titre de la boite",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);
                if (choix == JOptionPane.OK_OPTION) {
                    String valeur = JOptionPane.showInputDialog(win, "Entrer une valeur", 123);
                    System.out.println(valeur);
                }

            }
        });

        // BoutonAction => classe qui implémente l'interface ActionListener
        BoutonAction ba = new BoutonAction();
        bp1.addActionListener(ba);
        bp2.addActionListener(ba);

        // Action Listener => Associer une action à un composant
        bp1.setActionCommand("bouton 1");
        bp2.setActionCommand("bouton 2");

        // MouseAdapter => class abstraite qui implémente MouseListener
        // évite de redéfinir toutes les méthodes
        bp1.addMouseListener(new MouseAdapter() {

            // Affiche dans la console les coordonnées de la souris
            // lorsque le pointeur de la souris entre dans le bouton bp1
            @Override
            public void mouseEntered(MouseEvent e) {
                System.out.println(e.getX());
            }
        });

        bp1.addKeyListener(new KeyAdapter() {
            // Affiche dans la console la code et le caractère de la touche enfoncée
            // Quand le bouton à le focus
            @Override
            public void keyPressed(KeyEvent e) {
                System.out.println(e.getKeyCode() + " " + e.getKeyChar());
            }
        });

        // Rendre la fenêtre visible
        win.setVisible(true);
    }

}
