package fr.dawan.formation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

public class CalculatriceAction implements ActionListener {

    private JLabel affichage;

    private double resultat;

    private char operateur = ' ';

    private boolean clear = true;

    public CalculatriceAction(JLabel affichage) {
        this.affichage = affichage;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String affichageText = affichage.getText();
        switch (e.getActionCommand()) {
        case "0":
        case "1":
        case "2":
        case "3":
        case "4":
        case "5":
        case "6":
        case "7":
        case "8":
        case "9":
            if (clear) {
                affichage.setText(e.getActionCommand());
                clear = false;
            } else {
                affichage.setText(affichageText + e.getActionCommand());
            }
            break;

        case "<=":
            int s = affichageText.length();
            if (s > 1) {
                affichage.setText(affichageText.substring(0, s - 1));
            } else {
                affichage.setText("0");
                clear = true;
            }
            break;

        case "+/-":
            if (affichageText.length() > 0) {
                if (affichageText.charAt(0) == '-') {
                    affichage.setText(affichageText.substring(1));
                } else {
                    affichage.setText("-" + affichageText);
                }
                if (clear) {
                    resultat = -resultat;
                }
            }
            break;

        case ",":
            if (!affichageText.contains(".")) {
                affichage.setText(affichageText + ".");
            }
            break;

        case "CE":
            affichage.setText("0");
            break;

        case "C":
            affichage.setText("0");
            resultat = 0.0;
            operateur = ' ';
            clear = true;
            break;

        case "+":
        case "-":
        case "x":
        case "/":
        case "=":
            if (!clear && affichageText.length() > 0) {
                affichage.setText(calcul(affichageText));
                clear = true;
            }
            operateur = e.getActionCommand().charAt(0);
            break;
        }
    }
    
    /**
     *  Méthode pour réaliser le calcul
     * @param value La chaine de caractère qui contient le nombre
     * @return Le résultat
     */
    private String calcul(String value) {
        double val = Double.parseDouble(value);
        switch (operateur) {
        case '+':
            resultat += val;
            break;
        case '-':
            resultat -= val;
            break;
        case 'x':
            resultat *= val;
            break;
        case '/':
            resultat /= val;
            break;
        case ' ':
            resultat = val;
            break;
        }
        if (resultat == Math.floor(resultat)) {
            String tmp = Double.toString(resultat);
            return tmp.substring(0, tmp.length() - 2);
        } else {
            return Double.toString(resultat);
        }
    }
}
