package fr.dawan.formation;

import java.time.LocalDate;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import fr.dawan.formation.beans.ContactGen;

public class ContactTableModel extends AbstractTableModel {


    private static final long serialVersionUID = 1L;
    
    private List<ContactGen> data;
    
    
    public ContactTableModel(List<ContactGen> data) {
        super();
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ContactGen row=data.get(rowIndex);
        switch(columnIndex) {
        case 0:
            return row.getId();
        case 1:
            return row.getPrenom();
        case 2:
            return row.getNom();
        case 3:
            return row.getDateNaissance();
        case 4:
            return row.getEmail();
        }
        
        return null;
    }

    @Override
    public String getColumnName(int column) {
        switch(column) {
        case 0:
            return "Id";
        case 1:
            return "Prénom";
        case 2:
            return "Nom";
        case 3:
            return "Date Naissance";
        case 4:
            return "Email";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
        case 0:
            return Long.class;
        case 3:
            return LocalDate.class;
        default:
            return String.class;    
        }
    }
    

}
