
public class Main {

    public static void main(String[] args) {
        
        System.out.println(Thread.currentThread().getName());
        
        MyRunnable r=new MyRunnable();
        Thread t1=new Thread(r);
        t1.setName("T1");
        t1.setPriority(Thread.MIN_PRIORITY);
        
        MyThread t2=new MyThread();
        t2.setName("T2");
      //  t2.setDaemon(true);
        t1.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t2.start();
        for(int i=0;i<50;i++) {
            System.out.println("main "+ i);
        }

    }

}
